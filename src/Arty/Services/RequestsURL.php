<?php

namespace Arty\Services;

use PDO;
use PDOException;
use Exception;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class RequestsURL
{
    private const HTTP = "http://";
    private const HTTPS = "https://";
    private const HTTP_CHECK = "http";
    private string $tableName;
    private PDO $PDO;
    private Logger $logger;
    private string $shortURL;

    public function __construct(PDO $connectDB, string $tableName, Logger $logger)
    {
        $this->PDO = $connectDB;
        $this->tableName = $tableName;
        $this->logger = $logger;
    }

    /**
     * Проверка введёного url пользователем
     * @param string $url
     * @return bool
     */
    private function checkInputURL(string $url): bool
    {
        try {
            if (!str_contains($url, self::HTTP_CHECK)) {
                throw new Exception("Не указан протокол передачи данных");
            }
            if (
                str_contains($url, self::HTTPS . $_SERVER["SERVER_NAME"]) ||
                str_contains($url, self::HTTP . $_SERVER["SERVER_NAME"])
            ) {
                throw new Exception("Нельзя создавать ссылки на этот сайт");
            }
            if (!preg_match("/^[a-zA-Z]+[:\/\/]+[A-Za-z0-9\-_]+\\.+[A-Za-z0-9\.\/%&=\?\-_]+$/i", $url)) {
                throw new Exception("Не допустимый URL");
            }
            return true;
        } catch (Exception $e) {
            $this->logger->pushHandler(new StreamHandler($_SERVER["DOCUMENT_ROOT"] . "/logs/log_db_create.log", Logger::CRITICAL));
            $this->logger->info(
                "Ошибка при проверке нового URL: " . "Exception " . $e->getMessage()
            );
            return false;
        }
    }

    /**
     * Проверяем какой протокол был указан в url
     * @param string $url
     * @return string
     */
    public static function checkProtocolURL(string $url): string
    {
        if (str_contains($url, self::HTTPS)) {
            return self::HTTPS;
        } else {
            return self::HTTP;
        }
    }

    /**
     * Запись нового url в бд
     * @param string $url
     * @return bool|Exception
     */
    public function setNewURL(string $url): bool|PDOException
    {
        try {
            if (!$this->checkInputURL($url)) {
                return false;
            }
            $this->createShortURL();
            $sql = "INSERT INTO $this->tableName (url, new_url) VALUES (:url, :new_url)";
            $stmt = $this->PDO->prepare($sql);
            $stmt->execute([":url" => $url, ":new_url" => $this->shortURL]);
            return true;
        } catch (PDOException $e) {
            $this->logger->pushHandler(new StreamHandler($_SERVER["DOCUMENT_ROOT"] . "/logs/log_db_create.log", Logger::CRITICAL));
            $this->logger->info(
                "Ошибка при установке нового URL: " . ["PDOException" => $e->getMessage()]
            );
            throw new PDOException("Ошибка при установке нового URL: " . $e->getMessage());
        }
    }

    /**
     * Создание короткого url
     */
    private function createShortURL(): void
    {
        $this->shortURL = substr(md5(time()), 0, 8);
    }

    /**
     * Получение короткого url
     * @param string $protocol
     * @return string
     */
    public function getNewURL(string $protocol): string
    {
        return $protocol . $_SERVER["SERVER_NAME"] . "/" .  $this->shortURL;
    }

    /**
     * Создание редиректов url
     * @param string $requestShort
     */
    public function setRedirectURL(string $requestShort): void
    {
        try {
            $sql = "SELECT * FROM $this->tableName WHERE new_url = :url";
            $stmt = $this->PDO->prepare($sql);
            $stmt->execute([":url" => $requestShort]);
            if ($stmt->rowCount() > 0) {
                foreach ($stmt as $row) {
                    $url = $row["url"];
                    Header("HTTP/1.1 301 Moved Permanently");
                    header("Location: " . $url . "");
                }
            }
        } catch (PDOException $e) {
            $this->logger->pushHandler(new StreamHandler($_SERVER["DOCUMENT_ROOT"] . "/logs/log_db_create.log", Logger::CRITICAL));
            $this->logger->info(
                "Ошибка при создании редиректа: " . $requestShort . ": " . ["PDOException" => $e->getMessage()]
            );
        }
    }
}
