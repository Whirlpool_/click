<?php


// укажите свои параметры для базы данных
$database = [
    "NAME_DB" => "Click",
    "NAME_USER" => "root",
    "PASSWORD" => "root",
    "HOST" => "localhost",
    "TABLE_NAME" => "URL"
];

// данные для отправки сообщений на почту в случае критической ошибки приложения
$mailParams = [
    "SMTP" => "smtp.example.com", // SMTP-сервер
    "SMTP_AUTHENTICATION" => true, // Включить проверку подлинности SMTP
    "SENDER_NAME" => "click@click-site.com", // Имя пользователя SMTP
    "RECIPIENT_NAME" => "test@click-site.com", // почта получателя
    "PASSWORD" => "secret", // Пароль SMTP
    "PORT" => 0 // TCP-порт для подключения
];
