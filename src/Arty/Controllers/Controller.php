<?php


namespace Arty\Controllers;


use Exception;

abstract class Controller
{
    /**
     * @throws Exception
     */
    public static function render(string $template, array $params = []): void
    {
        if (!isset($template) || empty($template)) {
            throw new Exception("Не указан шаблон");
        }

        $path = $_SERVER["DOCUMENT_ROOT"] . "/template/" . $template . ".php";

        if (!file_exists($path)) {
            throw new Exception("Не найден файл шаблона");
        }

        extract($params);

        require_once($path);
    }
}
