<?php

require_once($_SERVER["DOCUMENT_ROOT"] . "/template/main/header.php");
?>
    <div class="container text-center">
        <div class="row g-3 mb-4">
            <div class="h1">Кликер</div>
            <form action="<?= htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="post">
                <div class="mb-3">
                    <div class="col">
                        <label for="url" class="form-label"><?php
                            if (isset($newURL)) {
                                echo "Ваш новый url";
                            } else {
                                echo "Введите URL";
                            }
                            ?>
                        </label>
                        <input type="text" name="url" class="form-control" id="url" value="<?php
                        if (isset($newURL)) {
                            echo $newURL;
                        } ?>">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Сократить</button>
                <?php
                if (isset($new_url)) { ?>
                    <a class="btn btn-primary" href="/">Обновить</a>
                    <?php
                } ?>
            </form>
        </div>
        <?php
        if (isset($newURL)) { ?>
            <div class="row">
                <div class="col-12">
                    <p class="mb-4">Новая ссылка запомнена, теперь вы можете скопировать и отправить адрес своим друзьям
                        и
                        знакомым. </p>
                    <?php
                    if ($QR) {
                        echo $QR;
                    }
                    ?>
                </div>
            </div>
            <?php
        } ?>
    </div>
<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/template/main/footer.php");
