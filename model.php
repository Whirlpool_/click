<?php


use Arty\Services\Database;
use Arty\Services\MailSend;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use PHPMailer\PHPMailer\Exception;


$logger = new Logger("logs");
if (count($mailParams) === 5) {
    $mail = new MailSend(
        $mailParams["SMTP"],
        $mailParams["SMTP_AUTHENTICATION"],
        $mailParams["SENDER_NAME"],
        $mailParams["RECIPIENT_NAME"],
        $mailParams["PASSWORD"],
        $mailParams["PORT"]
    );
}
try {
    if (count($database) !== 5) {
        throw new Exception("Настройте подключение к базе данных в файле config.php");
    }
    try {
        $db = Database::getInstance();
        $dbCon = $db->connect(
            $database["NAME_DB"],
             $database["NAME_USER"],
             $database["PASSWORD"],
             $database["HOST"],
             $database["TABLE_NAME"],
             $logger
        );
    } catch (PDOException $exception) {
        try {
            if ($mail) {
                $mailer = $mail->sendMailErrorDB($exception);
                $logMail = new Logger("mail");
                $logMail->pushHandler(new StreamHandler($_SERVER["DOCUMENT_ROOT"] . "/logs/log_mail.log", Logger::INFO));
                $logMail->info(
                    "Сообщение об ошибке подключения бд отправленно. Ошибка PHPMailer: " . ["PHPMailer" => $mailer->ErrorInfo]
                );
            }
        } catch (Exception $e) {
            $logMail = new Logger("mail");
            $logMail->pushHandler(new StreamHandler($_SERVER["DOCUMENT_ROOT"] . "/logs/log_mail.log", Logger::ALERT));
            $logMail->info(
                "Сообщение не удалось отправить." . ["Exception" => $e->getMessage()]
            );
        }

        $logger->pushHandler(new StreamHandler($_SERVER["DOCUMENT_ROOT"] . "/logs/log_db_connect.log", Logger::CRITICAL));
        $logger->info("Ошибка соединения базы данных: " . ["PDOException" => $exception->getMessage()]);
    }
} catch (Exception $e) {
    $logger->pushHandler(new StreamHandler($_SERVER["DOCUMENT_ROOT"] . "/logs/log_db_connect.log", Logger::CRITICAL));
    $logger->info("Ошибка подключения: " . ["Exception" => $e->getMessage()]);
}
