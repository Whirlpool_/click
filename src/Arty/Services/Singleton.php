<?php


namespace Arty\Services;


class Singleton
{
    public static self|null $instance = null;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    /**
     * Возвращает соединения с бд
     * @return static
     */
    public static function getInstance(): static
    {
        if (static::$instance === null) {
            static::$instance = new static;
        }
        return static::$instance;
    }
}