<?php


namespace Arty\Services;

use PDOException;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;


class MailSend
{
    private string $SMTP;
    private bool $SMTPAuth;
    private string $senderName;
    private string $password;
    private int $port;
    private string $recipientName;

    public function __construct(string $SMTP, bool $SMTPAuth, string $senderName, string $password, int $port, string $recipientName)
    {
        $this->SMTP = $SMTP;
        $this->SMTPAuth = $SMTPAuth;
        $this->senderName = $senderName;
        $this->password = $password;
        $this->port = $port;
        $this->recipientName = $recipientName;
    }

    public function sendMailErrorDB(PDOException $exception) : object
    {
        $mailer = new PHPMailer(true);
        $mailer->isSMTP();
        $mailer->Host = $this->SMTP;
        $mailer->SMTPAuth = $this->SMTPAuth;
        $mailer->Username = $this->senderName;
        $mailer->Password = $this->password;
        $mailer->Port = $this->port;
        $mailer->setFrom($this->recipientName, "Logging Server");
        $mailer->isHTML(true);
        $mailer->Subject = "Ошибка подключения базы данных у " . $_SERVER["SERVER_NAME"];
        $mailer->Body = "Ошибка соединения базы данных: " . $exception->getMessage();
        $mailer->send();
        return $mailer;
    }
}
