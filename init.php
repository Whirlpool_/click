<?php

if (!file_exists("config.php")) {
    throw new Exception ("Не найден файл config.php");
}
if (!file_exists("model.php")) {
    throw new Exception ("Не найден файл model.php");
}

require_once $_SERVER["DOCUMENT_ROOT"] . "/config.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/vendor/autoload.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/model.php";
