<?php

namespace Arty\Services;

use PDO;
use PDOException;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Database extends Singleton
{
    private string $dataBase;
    private string $userName;
    private string $passWord;
    private string $host;
    private string $tableName;
    private Logger $logger;
    private PDO $PDO;

    /**
     * Устанавливает соединения с бд
     * @return PDO|PDOException
     */
    public function connect(
        string $dataBase,
        string $userName,
        string $passWord,
        string $host,
        string $tableName,
        Logger $logger
    ): PDO|PDOException
    {
        $this->dataBase = $dataBase;
        $this->host = $host;
        $this->userName = $userName;
        $this->passWord = $passWord;
        $this->tableName = $tableName;
        $this->logger = $logger;
        try {
             $this->PDO = new PDO(
                "mysql:host=" . $this->host . ";dbname=" . $this->dataBase,
                $this->userName,
                $this->passWord
            );
            $this->addTable();
            return $this->PDO;
        } catch (PDOException $exception) {
            $this->logger->pushHandler(new StreamHandler($_SERVER["DOCUMENT_ROOT"] . "/logs/log_db_create.log", Logger::CRITICAL));
            $this->logger->info(
                "Ошибка соединения с " . $this->dataBase . ": " . ["PDOException" => $exception->getMessage()]
            );
            throw new PDOException("Ошибка соединения с " . $this->dataBase . " - " . $exception->getMessage());
        }
    }



    /**
     * Проверка наличия таблицы в бд
     * @return bool
     */
    private function checkTable(): bool|PDOException
    {
        try {
            $listTable = $this->PDO->query("SHOW TABLES")->fetchAll();
            foreach ($listTable as $elName) {
                if (in_array($this->tableName, $elName, true)) {
                    return true;
                }
            }
            return false;
        } catch (PDOException $exception) {
            $this->logger->pushHandler(new StreamHandler($_SERVER["DOCUMENT_ROOT"] . "/logs/log_db_create.log", Logger::CRITICAL));
            $this->logger->info(
                "Ошибка при проверке таблицы " . $this->tableName . ": " . ["Exception" => $exception->getMessage()]
            );
            throw new PDOException("Ошибка при проверке таблицы " . $this->tableName . " - " . $exception->getMessage());
        }
    }


    /**
     * Создание таблицы с столбцами id, url, new_url
     * @return bool
     */
    private function addTable(): bool
    {
        try {
            $checkTable = $this->checkTable();
            if ($checkTable) {
                return false;
            }
            $sql = "CREATE TABLE $this->tableName (
            id integer auto_increment primary key,
            url text not null,
            new_url varchar(8) unique not null);";
            $this->PDO->exec($sql);
            return true;
        } catch (PDOException $exception) {
            $this->logger->pushHandler(new StreamHandler($_SERVER["DOCUMENT_ROOT"] . "/logs/log_db_create.log", Logger::CRITICAL));
            $this->logger->info(
                "Ошибка присоздании таблицы " . $this->tableName . ": " . ["Exception" => $exception->getMessage()]
            );
            return false;
        }
    }
}
