<?php


namespace Arty\Controllers;


use Arty\Services\RequestsURL;
use Endroid\QrCode\Builder\Builder;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelHigh;
use Endroid\QrCode\Label\Alignment\LabelAlignmentCenter;
use Endroid\QrCode\Label\Font\NotoSans;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use PDO;
use PDOException;

class Main extends Controller
{
    private PDO $connect;
    private string $nameDB;
    private Logger $logger;

    public function __construct(PDO $connect, string $nameDB, Logger $logger)
    {
        $this->connect = $connect;
        $this->nameDB = $nameDB;
        $this->logger = $logger;
    }

    public function index()
    {
        $this->logger->pushHandler(
            new StreamHandler($_SERVER["DOCUMENT_ROOT"] . "/logs/log_db_connect.log", Logger::CRITICAL)
        );

        $RequestsURL = new RequestsURL($this->connect, $this->nameDB, $this->logger);

        if (isset($_REQUEST["short"])) {
            try {
                $RequestsURL->setRedirectURL($_REQUEST["short"]);
            } catch (PDOException $e) {
                $this->logger->info("Ошибка, не удалось осуществить редирект", ["PDOException" => $e->getMessage()]);
            }
        }
        if (isset($_POST["url"])) {
            try {
                $statusNewUrl = $RequestsURL->setNewURL($_POST["url"]);
                if (!$statusNewUrl) {
                    throw new PDOException("Ошибка при установке нового URL:");
                }
                $protocol = RequestsURL::checkProtocolURL($_POST["url"]);
                $newURL = $RequestsURL->getNewURL($protocol);
                $result = Builder::create()
                    ->writer(new PngWriter())
                    ->writerOptions([])
                    ->data($newURL)
                    ->encoding(new Encoding("UTF-8"))
                    ->errorCorrectionLevel(new ErrorCorrectionLevelHigh())
                    ->size(300)
                    ->margin(10)
                    ->roundBlockSizeMode(new RoundBlockSizeModeMargin())
                    ->logoPath($_SERVER["DOCUMENT_ROOT"] . "/assets/img/QR.jpg")
                    ->labelText("")
                    ->labelFont(new NotoSans(20))
                    ->labelAlignment(new LabelAlignmentCenter())
                    ->validateResult(false)
                    ->build();

                $dataUri = $result->getDataUri();
                $img = "<img src=" . $dataUri . ">";
                parent::render(
                    "index",
                    ["title" => "Click", "newURL" => $newURL, "QR" => $img]
                );
            } catch (PDOException $e) {
                $this->logger->info("Ошибка, не удалось создать редирект", ["PDOException" => $e->getMessage()]);
            }
        } else {
            parent::render("index", ["title" => "Click"]);
        }
    }
}
